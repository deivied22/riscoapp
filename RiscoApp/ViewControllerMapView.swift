//
//  ViewControllerMapView.swift
//  RiscoApp
//
//  Created by David Rodríguez Mora on 28/2/22.
//

import UIKit
import MapKit

class ViewControllerMapView: UIViewController {

    @IBOutlet weak var myMap: MKMapView!
 
    override func viewDidLoad() {
        super.viewDidLoad()
    
        title = "Mapa y Puntos de interés"
        
        //        Ubicaciones
               
         let riscoLocation = CLLocationCoordinate2D(latitude: 38.910296471984466,  longitude: -5.122451362115355)
        
                let viewRegion = MKCoordinateRegion(center: riscoLocation, latitudinalMeters: 1000, longitudinalMeters: 1000)
                    myMap.setRegion(viewRegion, animated:false)
                    myMap.showsUserLocation = true
        
                let townHall = MKPointAnnotation()
                    townHall.title = "Ayuntamiento"
                    townHall.coordinate = CLLocationCoordinate2D(latitude: 38.91080880117823, longitude: -5.122208806252927)
                    myMap.addAnnotation(townHall)
                            
                let catholicChurch = MKPointAnnotation()
                    catholicChurch.title = "Parroquia de San Blas"
                    catholicChurch.coordinate = CLLocationCoordinate2D(latitude: 38.910318145905634, longitude: -5.122475842704685)
                    myMap.addAnnotation(catholicChurch)
                
                let bar = MKPointAnnotation()
                    bar.title = "Bar"
                    bar.coordinate = CLLocationCoordinate2D(latitude: 38.91056665363571, longitude: -5.121453000135688)
                    myMap.addAnnotation(bar)
                
                let catholicHermitage = MKPointAnnotation()
                    catholicHermitage.title = "Ermita de la Buena Dicha"
                    catholicHermitage.coordinate = CLLocationCoordinate2D(latitude: 38.9111301684287, longitude: -5.122228158539805)
                    myMap.addAnnotation(catholicHermitage)
                
                let sportsCenter = MKPointAnnotation()
                    sportsCenter.title = "Polideportivo (Las Heras)"
                    sportsCenter.coordinate = CLLocationCoordinate2D (latitude: 38.90964716148679, longitude: -5.117939105579325)
                    myMap.addAnnotation(sportsCenter)
                
                let picnicArea = MKPointAnnotation()
                    picnicArea.title = "El Mato"
                    picnicArea.coordinate = CLLocationCoordinate2D(latitude: 38.90858413025674, longitude: -5.129529676663707)
                    myMap.addAnnotation(picnicArea)
               }

            func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
                guard annotation is MKPointAnnotation else { return nil }

                let identifier = "Annotation"
                var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)

                if annotationView == nil {
                    annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                    annotationView!.canShowCallout = true
                } else {
                    annotationView!.annotation = annotation
                }

               return annotationView
        
            }
        
    }
 
